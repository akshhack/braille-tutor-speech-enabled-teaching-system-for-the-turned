#include <SoftwareSerial.h>

const int solenoidPin1 = 13;
const int solenoidPin2 = 12;

String alphabetArray[26] = {
  "100000", //A
  "101000", //B
  "110000", //C
  "110100", //D
  "100100", //E
  "111000", //F
  "111100", //G
  "101100", //H
  "011000", //I
  "011100", //J
  "100010", //K
  "101010", //L
  "110010", //M
  "110110", //N
  "100110", //O
  "111010", //P
  "111110", //Q
  "101110", //R
  "011010", //S
  "011110", //T
  "100011", //U
  "101011", //V
  "011101", //W
  "110011", //X
  "110111", //Y
  "100111"  //Z
};

SoftwareSerial mySerial(2, 4); // RX, TX

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  while(!Serial) {
    ;
  }

  Serial.println("Begin");
  pinMode(solenoidPin1, OUTPUT);
  pinMode(solenoidPin2, OUTPUT);
  mySerial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (mySerial.available()) {
    Serial.println(alphabetArray[(mySerial.read() - 65)]);
    digitalWrite(solenoidPin1,HIGH);
    digitalWrite(solenoidPin2,LOW);
    delay(4900);
    digitalWrite(solenoidPin2,HIGH);
    digitalWrite(solenoidPin1,LOW);
    delay(100);
  }
}
