package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cmu.akshat.brailletutorandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class TutorialMainPage extends Fragment {

    private BtSteupContract.OnFragmentInteractionListener mListener;
    private TextToSpeech tts;
    private volatile boolean isTest = false;
    private final String TEST = "test";
    private final String LEARN = "learn";
    private final String ALPHABETS = "alphabets";
    private final String ALPHABET = "alphabet";
    private final String NUMBERS = "numbers";
    private final String REFERENCE = "refer";
    private final String TUTORIAL_CHOICE = "Choose to Learn, Test or Refer";
    private final String ALPHABETS_OR_NUMBERS = "Choose Alphabets or Numbers";
    private String PROMPT = TUTORIAL_CHOICE;
    private final String TUTORIAL_WELCOME_PROMPT =
            "Welcome to Braille Tutor. You can choose to Learn, Test or Refer. Speak at the beep";
    private final String TEST_PROMPT = "Do you wish to test numbers or alphabets?";
    private final String LEARN_PROMPT = "Do you wish to learn numbers or alphabets?";

    public TutorialMainPage() {
        // Required empty public constructor
    }

    public static TutorialMainPage newInstance() {
        return new TutorialMainPage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_tutorial_main_page, container, false);
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String s) {
                            //Do nothing.
                        }

                        @Override
                        public void onDone(String s) {
                            listen();
                        }

                        @Override
                        public void onError(String s) {
                            //Do nothing.
                        }
                    });
                    speak(TUTORIAL_WELCOME_PROMPT, System.currentTimeMillis());
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BtSteupContract.OnFragmentInteractionListener) {
            mListener = (BtSteupContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        mListener = null;
    }

    private void listen(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, PROMPT);

        try {
            startActivityForResult(i, BtSetupActivity.ActivityResultCode.TUTORIAL_MAIN_PAGE.rawValue());
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), "Your device doesn't support Speech Recognition",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BtSetupActivity.ActivityResultCode.TUTORIAL_MAIN_PAGE.rawValue()){
            if (resultCode == RESULT_OK && null != data) {
                ArrayList<String> res = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String inSpeech = res.get(0);
                recognition(inSpeech);
            }
        }
    }

    private void recognition(String text){
        switch(text.toLowerCase()) {
            case ALPHABET:
            case ALPHABETS:
                if (isTest) {
                    mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_TWO,
                            BtSetupActivity.ActivityMode.ALPHABETS);
                } else {
                    mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_ONE,
                            BtSetupActivity.ActivityMode.ALPHABETS);
                }
                break;
            case NUMBERS:
                if (isTest) {
                    mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_TWO,
                            BtSetupActivity.ActivityMode.NUMBERS);
                } else {
                    mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_ONE,
                            BtSetupActivity.ActivityMode.NUMBERS);
                }
                break;
            case REFERENCE:
                mListener.performUIAction(BtSetupActivity.UiActions.GOTO_REFERENCE, null);
                break;
            case TEST:
                PROMPT = ALPHABETS_OR_NUMBERS;
                isTest = true;
                speak(TEST_PROMPT, System.currentTimeMillis());
                //mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_TWO, null);
                break;
            case LEARN:
            default:
                PROMPT = ALPHABETS_OR_NUMBERS;
                isTest = false;
                speak(LEARN_PROMPT, System.currentTimeMillis());
                //mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_ONE, null);
                break;
        }
        Toast.makeText(getActivity(), "Response is: " + text,
                Toast.LENGTH_SHORT).show();
    }

    private void speak(String text, long id){
        HashMap<String, String> hm = new HashMap<>();
        hm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        Bundle bundle = new Bundle();
        bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, String.valueOf(id));
        }else{
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, hm);
        }
    }
}
