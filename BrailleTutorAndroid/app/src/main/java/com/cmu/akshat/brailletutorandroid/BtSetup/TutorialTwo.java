package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cmu.akshat.brailletutorandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class TutorialTwo extends Fragment {
    private BtSteupContract.OnFragmentInteractionListener mListener;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private static final String IS_ALPHABET_KEY = "isAlphabet";
    private volatile boolean isAlphabet = false;
    private volatile String character;
    private final String TAG = "TUTTWO";
    private final String WHICH_CHARACTER = "Which %s?";
    private final String TUTORIAL_TWO_WELCOME = "Tutorial Two. I will present a series of %s" +
            "on the braille pad and your job is to identify them.";
    private long waitTime = 5000;
    private TextToSpeech tts;

    private Runnable toastLetter = new Runnable() {
        @Override
        public void run() {
            Toast.makeText(getActivity(), "Prompt: " + character, Toast.LENGTH_SHORT).show();
        }
    };


    public TutorialTwo() {
        // Required empty public constructor
    }

    public static TutorialTwo newInstance(BtSetupActivity.ActivityMode activityMode) {
        TutorialTwo frag = new TutorialTwo();
        Bundle args = new Bundle();
        if (activityMode == BtSetupActivity.ActivityMode.ALPHABETS) {
            args.putBoolean(IS_ALPHABET_KEY, true);
        } else {
            args.putBoolean(IS_ALPHABET_KEY, false);
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isAlphabet = getArguments().getBoolean(IS_ALPHABET_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_tutorial_two, container, false);
        Button exitButton = mView.findViewById(R.id.tutorialTwoExit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_MAIN_PAGE, null);
            }
        });
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String s) {
                            //Do nothing.
                        }

                        @Override
                        public void onDone(String s) {
                            startExercise();
                        }

                        @Override
                        public void onError(String s) {
                            //Do nothing.
                        }
                    });

                    String toSpeak;
                    toSpeak = (isAlphabet ? (String.format(TUTORIAL_TWO_WELCOME, "alphabets")) :
                            (String.format(TUTORIAL_TWO_WELCOME, "numbers")));
                    speak(toSpeak, System.currentTimeMillis());
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BtSteupContract.OnFragmentInteractionListener) {
            mListener = (BtSteupContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        mListener = null;
    }

    private void startExercise() {
        Random random = new Random();
        char randomCharacter;
        randomCharacter = (isAlphabet ? ((char) (65 + random.nextInt(26))) :
                                        ((char) (48 + random.nextInt(10))));
        character = String.valueOf(randomCharacter);
        uiHandler.post(toastLetter);
        mListener.sendBtData(String.valueOf(randomCharacter));
        listen();
    }

    private void speak(String text, long id){
        HashMap<String, String> hm = new HashMap<>();
        hm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        Bundle bundle = new Bundle();
        bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, String.valueOf(id));
        }else{
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, hm);
        }
    }

    private void listen(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, String.format(WHICH_CHARACTER,
                  (isAlphabet ? "alphabet" : "number")));

        try {
            startActivityForResult(i, BtSetupActivity.ActivityResultCode.TUTORIAL_TWO.rawValue());
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), "Your device doesn't support Speech Recognition",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BtSetupActivity.ActivityResultCode.TUTORIAL_TWO.rawValue()){
            if (resultCode == RESULT_OK && null != data) {
                ArrayList<String> res = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String inSpeech = res.get(0);
                recognition(inSpeech);
            }
        }
    }

    private void recognition(String text){
        Toast.makeText(getActivity(), "recognized: " + text, Toast.LENGTH_SHORT).show();
        Log.d(TAG, "recognition: character is = " + character);
        Log.d(TAG, "recognition: recognized is = " + character);

        speak(correctOrIncorrect(text), System.currentTimeMillis());
    }

    private String correctOrIncorrect(String recog) {
        if (recog.toLowerCase().contains(character.toLowerCase())) {
            return "Correct";
        } else {
            switch(character.toLowerCase()) {
                case "2":
                    if (recog.toLowerCase().equals("too") || recog.toLowerCase().equals("to"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "4":
                    if (recog.toLowerCase().equals("for"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "8":
                    if (recog.toLowerCase().equals("ain't") || recog.toLowerCase().equals("aight")
                            || recog.toLowerCase().equals("ate"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "a":
                    if (recog.toLowerCase().equals("hey"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "b":
                    if (recog.toLowerCase().equals("be") || recog.toLowerCase().equals("bee"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "c":
                    if (recog.toLowerCase().equals("see"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "f":
                    if (recog.toLowerCase().equals("half") || recog.toLowerCase().equals("if"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "g":
                    if (recog.toLowerCase().equals("gee"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "h":
                    if (recog.toLowerCase().equals("bench"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "i":
                    if (recog.toLowerCase().equals("hi"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "j":
                    if (recog.toLowerCase().equals("jake"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "k":
                    if(recog.toLowerCase().equals("gay") || recog.toLowerCase().equals("take") ||
                            recog.toLowerCase().equals("okay"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "l":
                    if(recog.toLowerCase().equals("oh"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "n":
                    if (recog.toLowerCase().equals("and"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "o":
                    if (recog.toLowerCase().equals("oh"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "p":
                    if(recog.toLowerCase().equals("pea") || recog.toLowerCase().equals("pee"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "q":
                    if(recog.toLowerCase().equals("cute"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "r":
                    if(recog.toLowerCase().equals("are"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "s":
                    if(recog.toLowerCase().equals("yes"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "u":
                    if(recog.toLowerCase().equals("you"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "v":
                    if(recog.toLowerCase().equals("free"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "x":
                    if(recog.toLowerCase().equals("ex"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "y":
                    if(recog.toLowerCase().equals("why"))
                        return "Correct";
                    else
                        return "Incorrect";
                case "z":
                    if(recog.toLowerCase().equals("lee") || recog.toLowerCase().equals("the"))
                        return "Correct";
                    else
                        return "Incorrect";
                default:
                    // Do nothing.
                    return "Incorrect";
            }
        }
    }
}
