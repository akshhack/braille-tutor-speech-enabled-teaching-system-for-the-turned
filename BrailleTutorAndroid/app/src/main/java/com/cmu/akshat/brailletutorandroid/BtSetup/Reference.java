package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.cmu.akshat.brailletutorandroid.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import static android.app.Activity.RESULT_OK;

public class Reference extends Fragment {

    private BtSteupContract.OnFragmentInteractionListener mListener;
    private static final String TAG = "REF";
    private long waitTime = 3000;
    private boolean hasIntroduced = false;
    private final String WHICH_CHARACTER = "Which character?";
    private final String REFERENCE_WELCOME = "Braille Reference. Speak a character and " +
            "refer to it's representation on the braille pad";
    private TextToSpeech tts;

    public Reference() {
        // Required empty public constructor
    }

    public static Reference newInstance() {
        return new Reference();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View mView = inflater.inflate(R.layout.fragment_reference, container, false);
        Button exitButton = (Button) mView.findViewById(R.id.referenceExit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_MAIN_PAGE, null);
            }
        });
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String s) {
                            //Do nothing.
                        }

                        @Override
                        public void onDone(String s) {
                            if (!hasIntroduced) {
                                listen();
                                hasIntroduced = true;
                            } else {
                                try {
                                    Thread.sleep(waitTime);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                listen();
                            }
                        }

                        @Override
                        public void onError(String s) {
                            //Do nothing.
                        }
                    });

                    speak(REFERENCE_WELCOME, System.currentTimeMillis());
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
        return mView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BtSteupContract.OnFragmentInteractionListener) {
            mListener = (BtSteupContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        mListener = null;
    }

    private void speak(String text, long id){
        HashMap<String, String> hm = new HashMap<>();
        hm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        Bundle bundle = new Bundle();
        bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, String.valueOf(id));
        }else{
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, hm);
        }
    }

    private void listen(){
        Intent i = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        i.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        i.putExtra(RecognizerIntent.EXTRA_PROMPT, WHICH_CHARACTER);

        try {
            startActivityForResult(i, BtSetupActivity.ActivityResultCode.REFERENCE.rawValue());
        } catch (ActivityNotFoundException a) {
            Toast.makeText(getActivity(), "Your device doesn't support Speech Recognition",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == BtSetupActivity.ActivityResultCode.REFERENCE.rawValue()){
            if (resultCode == RESULT_OK && null != data) {
                ArrayList<String> res = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String inSpeech = res.get(0);
                recognition(inSpeech);
            }
        }
    }

    private void recognition(String text){
        char first = trueCharacter(text);
        mListener.sendBtData(String.valueOf(first));
        speak((first + " is displayed on your braille pad"), System.currentTimeMillis());
        Toast.makeText(getActivity(), "recognized: " + first, Toast.LENGTH_SHORT).show();
    }

    private char trueCharacter(String recog) {
        switch(recog.toLowerCase()) {
            case "too":
            case "to":
                return '2';
            case "for":
                return '4';
            case "ain't":
            case "ate":
            case "aight":
                return '8';
            case "hey":
                return 'A';
            case "be":
            case "bee":
                return 'B';
            case "see":
                return 'C';
            case "half":
            case "if":
                return 'F';
            case "gee":
                return 'G';
            case "bench":
                return 'H';
            case "hi":
                return 'I';
            case "jake":
                return 'J';
            case "gay":
            case "take":
            case "okay":
                return 'K';
            case "and":
                return 'N';
            case "pea":
            case "pee":
                return 'P';
            case "cute":
                return 'Q';
            case "are":
                return 'R';
            case "yes":
                return 'S';
            case "you":
                return 'U';
            case "free":
                return 'V';
            case "ex":
                return 'X';
            case "why":
                return 'Y';
            case "lee":
            case "the":
                return 'Z';
            default:
                return Character.toUpperCase(recog.charAt(0));

        }
    }
}
