package com.cmu.akshat.brailletutorandroid.Splash;

public interface SplashContract {

    interface SplashViewOperations {
        void redirectUser();
    }

    interface SplashPresenterOperations {
        void redirectUser();
    }

    interface SplashModelOperations {

    }
}
