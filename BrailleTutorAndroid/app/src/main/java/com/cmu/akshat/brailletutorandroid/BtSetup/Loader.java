package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.cmu.akshat.brailletutorandroid.R;
import com.victor.loading.rotate.RotateLoading;

public class Loader extends Fragment {

    private BtSteupContract.OnFragmentInteractionListener mListener;
    private RotateLoading rotateLoading;

    public Loader() {
        // Required empty public constructor
    }

    public static Loader newInstance() {
        return new Loader();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loader, container, false);
        rotateLoading = view.findViewById(R.id.rotateLoading);
        if (rotateLoading != null && !rotateLoading.isStart()) {
            rotateLoading.start();
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BtSteupContract.OnFragmentInteractionListener) {
            mListener = (BtSteupContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (rotateLoading != null && rotateLoading.isStart()) {
            rotateLoading.stop();
        }
        mListener = null;
    }
}
