package com.cmu.akshat.brailletutorandroid.Splash;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.cmu.akshat.brailletutorandroid.BtSetup.BtSetupActivity;
import com.cmu.akshat.brailletutorandroid.R;
import com.cmu.akshat.brailletutorandroid.Toolbox.FontManager;

public class SplashActivity extends AppCompatActivity implements SplashContract.SplashViewOperations{

    private Handler uiHandler;
    private Runnable redirectUserRunnable;
    private SplashContract.SplashPresenterOperations splashPresenterOperations;
    private TextView splashItem;
    private final long ANIMATION_WAIT_TIME = 2500;

    public SplashActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setUpSplashItem();
        splashPresenterOperations = new SplashPresenter(this);
        uiHandler = new Handler(Looper.getMainLooper());
        redirectUserRunnable = new Runnable() {
            @Override
            public void run() {
                splashPresenterOperations.redirectUser();
            }
        };
    }

    public void redirectUser() {
        Intent intent = new Intent(this, BtSetupActivity.class);
        startActivity(intent);
        finish();
    }

    private void setUpSplashItem() {
        splashItem = (TextView) findViewById(R.id.splashItem);
        splashItem.setTypeface(FontManager.getTypeface(this, FontManager.FONTAWESOME));
    }

    @SuppressWarnings("unused")
    private void startSplashItemAnimation() {
        Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
        if (splashItem != null) {
            splashItem.startAnimation(pulse);
        }
    }

    @Override
    protected void onResume() {
        //startSplashItemAnimation();
        if (uiHandler != null && redirectUserRunnable != null) {
            uiHandler.postDelayed(redirectUserRunnable, ANIMATION_WAIT_TIME);
        }
        super.onResume();
    }

    @Override
    protected void onPause() {
//        if (splashItem != null) {
//            splashItem.clearAnimation();
//        }

        if (uiHandler != null) {
            uiHandler.removeCallbacks(redirectUserRunnable);
        }
        super.onPause();
    }
}
