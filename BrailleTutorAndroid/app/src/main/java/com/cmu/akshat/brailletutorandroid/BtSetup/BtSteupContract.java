package com.cmu.akshat.brailletutorandroid.BtSetup;

import java.util.HashMap;
import java.util.List;

public interface BtSteupContract {

    interface BtSetupViewOperations {}
    interface BtSetupPresenterOperations {}
    interface BtSetupModelOperations {}
    interface OnFragmentInteractionListener {
        void sendBtData(String randomAlphabet);
        void performUIAction(BtSetupActivity.UiActions uiActions, Object data);
    }


}
