package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.cmu.akshat.brailletutorandroid.R;

import java.util.List;

import es.dmoral.toasty.Toasty;
import io.palaima.smoothbluetooth.Device;
import io.palaima.smoothbluetooth.SmoothBluetooth;

public class BtSetupActivity extends AppCompatActivity
        implements BtSteupContract.OnFragmentInteractionListener{

    SmoothBluetooth mSmoothBluetooth;
    private final String TAG = "BTSETUP";
    private final String TARGET_DEVICE = "Braille Tutor";
    private final String HC_05 = "HC-05";

    private FragmentTransaction waitingTransaction;
    private boolean doNotCommitTransaction;

    @Override
    public void sendBtData(String randomAlphabet) {
        Log.d(TAG, "sendBtData: called.");
        if (mSmoothBluetooth != null) {
            Log.d(TAG, "sendBtData: transmitted");
            mSmoothBluetooth.send(randomAlphabet);
        }
    }

    @Override
    public void performUIAction(UiActions uiAction, Object data) {
        switch (uiAction) {
            case GOTO_TUTORIAL_TWO:
                if (data instanceof ActivityMode) {
                    ActivityMode activityMode = (ActivityMode) data;
                    replaceFragment(TutorialTwo.newInstance(activityMode),
                            BtSetupFragmentType.TUTORIAL_TWO.rawValue(), false);
                }
                break;
            case GOTO_TUTORIAL_MAIN_PAGE:
                replaceFragment(new TutorialMainPage(),
                        BtSetupFragmentType.TUTORIAL_MAIN_PAGE.rawValue(), false);
                break;
            case GOTO_TUTORIAL_ONE:
                if (data instanceof ActivityMode) {
                    ActivityMode activityMode = (ActivityMode) data;
                    replaceFragment(TutorialOne.newInstance(activityMode),
                            BtSetupFragmentType.TUTORIAL_ONE.rawValue(), false);
                }
                break;
            case GOTO_REFERENCE:
                replaceFragment(Reference.newInstance(), BtSetupFragmentType.REFERENCE.rawValue(),
                        false);
                break;
            default:
                //Do nothing.
                break;
        }
    }

    public enum ActivityMode {
        TEST("test"),
        LEARN("learn"),
        ALPHABETS("alphabets"),
        NUMBERS("numbers");

        final String activityMode;

        ActivityMode(final String activityMode) {
            this.activityMode = activityMode;
        }

        String rawValue() {
            return this.activityMode;
        }
    }

    public enum ActivityResultCode {
        TUTORIAL_MAIN_PAGE(100),
        TUTORIAL_ONE(101),
        TUTORIAL_TWO(102),
        REFERENCE(103);

        final int activityResultCode;

        ActivityResultCode(final int activityResultCode) {
            this.activityResultCode = activityResultCode;
        }

        int rawValue() {return this.activityResultCode;}
    }

    public enum UiActions {
        GOTO_TUTORIAL_ONE("gotoTutorialOne"),
        GOTO_TUTORIAL_TWO("gotoTutorialTwo"),
        GOTO_TUTORIAL_MAIN_PAGE("gotoTutorialMainPage"),
        GOTO_REFERENCE("gotoReference");

        final String uiAction;

        UiActions(final String uiAction) {this.uiAction = uiAction;}

        String rawValue(){return this.uiAction;}
    }

    private enum BtSetupFragmentType {
        LOADER("loader"),
        TUTORIAL_MAIN_PAGE("tutorialMainPage"),
        TUTORIAL_ONE("tutorialOne"),
        TUTORIAL_TWO("tutorialTwo"),
        REFERENCE("reference");

        final String fragmentType;

        BtSetupFragmentType(final String fragmentType) {this.fragmentType = fragmentType;}

        String rawValue() {return this.fragmentType;}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSmoothBluetooth = new SmoothBluetooth(this,
                SmoothBluetooth.ConnectionTo.OTHER_DEVICE,
                SmoothBluetooth.Connection.SECURE,
                mListener);
        setContentView(R.layout.activity_bt_setup);

        // start up view
        if (savedInstanceState == null) {
            replaceFragment(new Loader(), BtSetupFragmentType.LOADER.rawValue(), false);
        }

        // try to connect
        mSmoothBluetooth.tryConnection();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSmoothBluetooth.stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        Log.d(TAG, "onSaveInstanceState: called.");
        super.onSaveInstanceState(outState);
        doNotCommitTransaction = true;
        waitingTransaction = null;
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onSaveInstanceState: called.");
        super.onPause();
        doNotCommitTransaction = true;
        waitingTransaction = null;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        doNotCommitTransaction = false;
        if (waitingTransaction != null) {
            try {
                waitingTransaction.commit();
            } catch (IllegalStateException ise) {
                ise.printStackTrace();
            }
            waitingTransaction = null;
        }
    }

    private void replaceFragment(Fragment frag, String fragTag, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.bt_setup_fragment_container, frag, fragTag);
        if (doNotCommitTransaction) {
            Log.d(TAG, "addFragment: fragment set to waiting.");
            waitingTransaction = transaction;
        } else {
            Log.d(TAG, "addFragment: fragment committed.");
            if (addToBackStack) {
                transaction.addToBackStack(fragTag);
            }
            transaction.commit();
        }
    }

    private SmoothBluetooth.Listener mListener = new SmoothBluetooth.Listener() {
        private int connectionTries = 0;

        @Override
        public void onBluetoothNotSupported() {
            Toasty.error(getApplicationContext(), "Device does not support bluetooth",
                    Toast.LENGTH_SHORT).show();
            finish();
        }

        @Override
        public void onBluetoothNotEnabled() {
            //bluetooth is disabled, probably call Intent request to enable bluetooth
            Toasty.warning(getApplicationContext(), "Please turn on bluetooth and return",
                    Toast.LENGTH_SHORT).show();
            finish();
        }

        @Override
        public void onConnecting(Device device) {
            //called when connecting to particular device
            if (device != null) {
                Toasty.info(BtSetupActivity.this, "Connecting to " +
                                (device.getName().equals(HC_05) ? TARGET_DEVICE : device.getName()),
                        Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.info(BtSetupActivity.this, "Connecting to " + TARGET_DEVICE,
                        Toast.LENGTH_SHORT, true).show();
            }
        }

        @Override
        public void onConnected(Device device) {
            if (device != null) {
                Toasty.success(BtSetupActivity.this, "Connected to " +
                                (device.getName().equals(HC_05) ? TARGET_DEVICE : device.getName()),
                        Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.success(BtSetupActivity.this, "Connected to " + TARGET_DEVICE,
                        Toast.LENGTH_SHORT, true).show();
            }
            connectionTries = 0;

            Log.d(TAG, "onConnected: connected.");
            
            // open tutorial main page.
            replaceFragment(new TutorialMainPage(), BtSetupFragmentType.TUTORIAL_MAIN_PAGE.rawValue(), false);
        }

        @Override
        public void onDisconnected() {
            //called when disconnected from device
            Toasty.info(getApplicationContext(), "Braille Tutor has been disconnected",
                    Toast.LENGTH_SHORT, true).show();
            mSmoothBluetooth.tryConnection();
        }

        @Override
        public void onConnectionFailed(Device device) {
            //called when connection failed to particular device
            if (device != null) {
                Toasty.error(BtSetupActivity.this, "Could not connect to " +
                                (device.getName().equals(HC_05) ? TARGET_DEVICE : device.getName()),
                        Toast.LENGTH_SHORT, true).show();
            } else {
                Toasty.error(BtSetupActivity.this, "Could not connect to " + TARGET_DEVICE,
                        Toast.LENGTH_SHORT, true).show();
            }
            
            if (connectionTries < 3) {
                mSmoothBluetooth.tryConnection();
                connectionTries++;
            } else {
                Toasty.error(BtSetupActivity.this, "Connection issues. Please try later",
                        Toast.LENGTH_SHORT, true).show();
                finish();
            }
        }

        @Override
        public void onDiscoveryStarted() {
            //called when discovery is started
            Log.d(TAG, "onDiscoveryStarted: started");
        }

        @Override
        public void onDiscoveryFinished() {
            //called when discovery is finished
            Log.d(TAG, "onDiscoveryFinished: finished");
        }

        @Override
        public void onNoDevicesFound() {
            //called when no devices found
            if (connectionTries < 3) {
                mSmoothBluetooth.tryConnection();
                connectionTries++;
            } else {
                Toasty.error(BtSetupActivity.this, "Connection issues. Please try later",
                        Toast.LENGTH_SHORT, true).show();
                finish();
            }
        }

        @Override
        public void onDevicesFound(final List<Device> deviceList,
                                   final SmoothBluetooth.ConnectionCallback connectionCallback) {
            //receives discovered devices list and connection callback

            // Connect to HC-05
            for (Device device : deviceList) {
                if (device.getName().equals(HC_05)) {
                    connectionCallback.connectTo(device);
                    return;
                }
            }

            Toasty.error(BtSetupActivity.this,  TARGET_DEVICE + " not found. Please try later",
                    Toast.LENGTH_SHORT, true).show();
            finish();
        }

        @Override
        public void onDataReceived(int data) {
            //receives all bytes
            // should never happen.
            Log.d(TAG, "onDataReceived: data = " + data);
        }
    };

    @Override
    public void onBackPressed() {
        moveTaskToBack(true);
    }
}
