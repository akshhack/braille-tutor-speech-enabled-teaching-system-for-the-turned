package com.cmu.akshat.brailletutorandroid.BtSetup;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cmu.akshat.brailletutorandroid.R;

import java.util.HashMap;
import java.util.Locale;
import java.util.Random;


public class TutorialOne extends Fragment {
    private BtSteupContract.OnFragmentInteractionListener mListener;

    private TextToSpeech tts;
    private static final String IS_ALPHABET_KEY = "isAlphabet";
    private boolean isAlphabet = false;
    private TextView brailleLetter;
    private volatile String character;
    private Handler uiHandler = new Handler(Looper.getMainLooper());
    private long waitTime = 5500;
    private final String TUTORIAL_ONE_WELCOME = "Tutorial One. Here we will go through a series " +
            "of %s. I will speak the %s and it will appear on the braille pad";

    private Runnable changeText = new Runnable() {
        @Override
        public void run() {
            brailleLetter.setText(character);
        }
    };

    public TutorialOne() {
        // Required empty public constructor
    }

    @SuppressWarnings("unused")
    public static TutorialOne newInstance(BtSetupActivity.ActivityMode activityMode) {
        TutorialOne frag = new TutorialOne();
        Bundle args = new Bundle();
        if (activityMode == BtSetupActivity.ActivityMode.ALPHABETS) {
            args.putBoolean(IS_ALPHABET_KEY, true);
        } else {
            args.putBoolean(IS_ALPHABET_KEY, false);
        }
        frag.setArguments(args);
        return frag;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isAlphabet = getArguments().getBoolean(IS_ALPHABET_KEY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutorial_one, container, false);
        Button exitButton = view.findViewById(R.id.tutorialOneExit);
        exitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (tts != null) {
                    tts.stop();
                    tts.shutdown();
                }
                mListener.performUIAction(BtSetupActivity.UiActions.GOTO_TUTORIAL_MAIN_PAGE, null);
            }
        });
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.US);
                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "This Language is not supported");
                    }
                    String toSpeak;
                    if (isAlphabet) {
                        toSpeak = String.format(TUTORIAL_ONE_WELCOME, "english alphabets", "alphabet");
                    } else {
                        toSpeak = String.format(TUTORIAL_ONE_WELCOME, "numbers", "number");
                    }
                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String s) {
                            //Do nothing.
                        }

                        @Override
                        public void onDone(String s) {
                            exercise();
                        }

                        @Override
                        public void onError(String s) {
                            //Do nothing.
                        }
                    });
                    speak(toSpeak, System.currentTimeMillis());
                } else {
                    Log.e("TTS", "Initilization Failed!");
                }
            }
        });
        brailleLetter = view.findViewById(R.id.brailleLetter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BtSteupContract.OnFragmentInteractionListener) {
            mListener = (BtSteupContract.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        mListener = null;
    }

    private void speak(String text, long id){
        HashMap<String, String> hm = new HashMap<>();
        hm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        Bundle bundle = new Bundle();
        bundle.putString(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, String.valueOf(id));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, bundle, String.valueOf(id));
        }else{
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, hm);
        }
    }

    private void exercise() {
            Random random = new Random();
            char randomCharacter;
            if (isAlphabet) {
                randomCharacter = (char) (65 + random.nextInt(26));
            } else {
                randomCharacter = (char) (48 + random.nextInt(10));
            }
            character = String.valueOf(randomCharacter);
            mListener.sendBtData(String.valueOf(randomCharacter));
            uiHandler.post(changeText);
            speak(String.valueOf(randomCharacter), System.currentTimeMillis());
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
    }
}
