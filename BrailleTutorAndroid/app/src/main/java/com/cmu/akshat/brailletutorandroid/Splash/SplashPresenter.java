package com.cmu.akshat.brailletutorandroid.Splash;

public class SplashPresenter implements SplashContract.SplashPresenterOperations {

    SplashContract.SplashViewOperations splashViewOperations;

    SplashPresenter(SplashContract.SplashViewOperations splashViewOperations) {
        this.splashViewOperations = splashViewOperations;
    }


    @Override
    public void redirectUser() {
        if (splashViewOperations != null) {
            splashViewOperations.redirectUser();
        }
    }
}
